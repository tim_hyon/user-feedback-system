# README #
# Haus Take Home Assignment: User Feedback System w/ Slack Integration #

# Backend #
* Used passport and JWTs to authenticate users.
* Tools: express, mongo, mongoose, passport

### Backend Models ###
* User - has username, date of last password change, and hashed password
* FeedbackPost - has subject, description, and createdBy which is a ref to User

### Backend Endpoints ###
* POST /auth/login - login w/ username + password and get back a JWT
* POST /auth/register - create a new user

* GET /users - get all users. Not really used in this prototype, but could be useful for future improvements i.e. an admin user that manages other users.
* GET /user/current - get the current authenticated user from the JWT.
* GET /user/id - send a user id and get back a user object.
* PUT /user/id/username - update the username.
* PUT /user/id/password - update the password.
* DELETE /users/id - delete a user.

* GET /feedback-posts - get all posts for authenticated users.
* POST /feedback-posts - create a feedback post.
* GET /feedback-posts/id - create a post.
* PUT /feedback-posts/id - update post's subject / description.
* DELETE /feedback-posts/id - create a post.

### Utils ###
* Auth - this contains code to set up the passport local + JWT strategy for authentication
* Slack - this contains code to generate the options for the slack webhook call. The utils are used on create + edit of feedback posts.

# Frontend #
* Tools: react, redux
* client/ contains all the code for the frontend

### Main Pages ###
* Login - combined form to login or create users
* User Profile - Update username or password
* Feedback Post List - combined page to create new feedback posts + display the user's previous posts
* Feedback Post Edit - form to edit a feedback post

# Deployment #
* Used webpack to bundle the frontend
* Deployed to a droplet on digital ocean which functions as the database server / api server
* Deployed as a single project repo w/ fronted + backend, w/ express serving the bundled webpack output

# Reflection #

### Time spent ###
* I did go over time a little bit but this was mostly due to deployment issues and setup on the digital ocean droplet.
* Overall I think my lessons learned were to start deploying earlier, maybe use a deployment service like heroku for a prototype, and maybe use a simpler authentication scheme for a prototype

### Known Issues / Things to improve ###
* User delete is implemented in the backend, but there is no UI to access it
* Forgot password / reset is not implemented, but I did not think it was a good use of time to set up an email flow given the time constraints
* I did not add any testing for the prototype, but of course this would be important for a more 'real life' project
* More consistent status codes

### Possible Future Improvements ###
* Allow comments on feedback from slack
* Create admin users / pages to manage users
* Use enhanced Slack formatting instead of just plain text output
* For backend - adding standard enhancements on endpoints i.e. pagination, searching, etc.
* For frontend - for the sake of time a lot of pages are loaded with mutliple purposes. These would ideally be seperate flows, pages, or modals so each pages isn't a jumble of different forms / functionality.

### Final Thoughts ###
* Overall I think this was a pretty fun project to work on. I learned a good amount about deploying on an empty server + JWT based authentication.
